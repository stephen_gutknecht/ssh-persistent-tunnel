source: http://commons.wikimedia.org/wiki/File:Victoria_Tunnel_%28Newcastle%29_near_Ouse_Street_2010-02-19.jpg

Description 	
English: A lit section of the Victoria Tunnel in Newcastle upon Tyne, photographed during a public tour. A sloping passage to the Ouse Street entrance joins from the left where the light on the ceiling is. This photo looks towards the dead end that used to lead down to the River Tyne. The rest of the tunnel is in the opposite direction, heading towards Spital Tongues, and is mostly unlit.
Date 	19 February 2010
Source 	Own work
Author 	CDC



This file is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license. 	

    You are free:

        to share – to copy, distribute and transmit the work
        to remix – to adapt the work

    Under the following conditions:

        attribution – You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
        share alike – If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

